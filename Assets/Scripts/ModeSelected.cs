﻿using UnityEngine;

public class ModeSelected : MonoBehaviour
{
    public static ModeSelected modeSelected;
    public bool isOnline;
    public bool isOfflineMultiplayer;

    private void OnEnable()
    {
        if (ModeSelected.modeSelected == null)
        {
            ModeSelected.modeSelected = this;
        }
        else
        {
            if (ModeSelected.modeSelected != this)
            {
                Destroy(ModeSelected.modeSelected.gameObject);
                ModeSelected.modeSelected = this;
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
 //       isOnline = false;
    }
}
