﻿using System.Collections;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

public class PhotonRoomMatchmakingAccess : MonoBehaviourPunCallbacks
{
    [SerializeField] private GameObject startButton;
    [SerializeField] private GameObject lobbyPanel;
    [SerializeField] private GameObject roomPanel;
    [SerializeField] private GameObject playerListingPrefab;
    [SerializeField] private Text roomNameDisplay;
    [SerializeField] private Transform playersContainer;

    public bool gameCanBeCloseToNewPlayer;  

    private void ClearPlayerListings()
    {
        for (int i = playersContainer.childCount - 1; i >= 0; i--)
        {
            Destroy(playersContainer.GetChild(i).gameObject);
        }
    }

    private void ListPlayers()
    {
        foreach (Player player in PhotonNetwork.PlayerList)
        {
            GameObject tempListing = Instantiate(playerListingPrefab, playersContainer);
            Text tempText = tempListing.transform.GetChild(0).GetComponent<Text>();
            tempText.text = player.NickName;
        }
    }

    public override void OnJoinedRoom()
    {
        roomPanel.SetActive(true);
        lobbyPanel.SetActive(false);
        roomNameDisplay.text = PhotonNetwork.CurrentRoom.Name;

        if (PhotonNetwork.IsMasterClient)
        {
            startButton.SetActive(true);
            if (PhotonNetwork.CurrentRoom.PlayerCount == PhotonNetwork.CurrentRoom.MaxPlayers)
            {
                startButton.GetComponent<Button>().interactable = true;
            }
            else
            {
                startButton.GetComponent<Button>().interactable = false;
            }
            
        }
        else
        {
            startButton.SetActive(false);
        }

        //photonPlayers = PhotonNetwork.PlayerList;
        ClearPlayerListings(); //remove all old player listings
        ListPlayers(); // relist all current player listings
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        ClearPlayerListings(); //remove all old player listings
        ListPlayers(); // relist all current player listings
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        ClearPlayerListings(); //remove all old player listings
        ListPlayers(); // relist all current player listings

        // if the local player is now the master client (host migration)
        if (PhotonNetwork.IsMasterClient)
        {
            startButton.SetActive(true);
        }
    }

    public void StartGame()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            // unable to join after the game start from the lobby panel
            if (gameCanBeCloseToNewPlayer)
            {
                PhotonNetwork.CurrentRoom.IsOpen = false;
                PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.multiplayerCharacterSelectScene);
            }
            else
            {
                PhotonNetwork.LoadLevel(MultiplayerSetting.multiplayerSetting.multiplayerCharacterSelectScene);
            }
        }
    }

    IEnumerator RejoinLobby()
    {
        yield return new WaitForSeconds(1);
        PhotonNetwork.JoinLobby();
    }

    public void BackButton()
    {
        if (PhotonNetwork.IsConnectedAndReady)
        {
            lobbyPanel.SetActive(true);
            roomPanel.SetActive(false);
            PhotonNetwork.LeaveRoom();
 //           PhotonNetwork.LeaveLobby();

//            StartCoroutine(RejoinLobby());
        }

    }
}
