﻿using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class CharacterSelectionController : MonoBehaviour
{
    private PhotonView PV;
    public Button[] buttons;

    private int[] nb = { -1, -1, -1, -1 };

    void Start()
    {
        PV = GetComponent<PhotonView>();
    }
    
    public void SelectCharacter(int characterIdentifier) {
        PV.RPC("OnClickCharacterPick", RpcTarget.AllBufferedViaServer, characterIdentifier);
        Debug.Log("I clicked on a Character number " + characterIdentifier);
    }

    [PunRPC]
    public void OnClickCharacterPick(int characterIdentifier)
    {
        if (PlayerInfo.playerInfo != null)
        {
            //Multiplayer-only debug log
            //Debug.Log("I tell others that I clicked on a Character number " + characterIdentifier);

            PlayerInfo.playerInfo.mySelectedCharacter = characterIdentifier;

            for (int i = 0; i < nb.Length; i++)
            {
                buttons[characterIdentifier].interactable = false;
                if (i == characterIdentifier)
                {
                    continue;
                }
                else
                {
                    buttons[i].interactable = true;
                }
            }
            PlayerPrefs.SetInt("LOCALCHARACTER", characterIdentifier);
        }
    }
}
