﻿using UnityEngine;

public class PlayerInfo : MonoBehaviour
{
    public static PlayerInfo playerInfo;

    public int mySelectedCharacter;
    public GameObject[] allCharacters;
    public string avatarPrefabSelected;
    public int spawnPrefabNumber;

    private void OnEnable()
    {
        if (PlayerInfo.playerInfo == null)
        {
            PlayerInfo.playerInfo = this;
        }
        else
        {
            if (PlayerInfo.playerInfo != this)
            {
                Destroy(PlayerInfo.playerInfo.gameObject);
                PlayerInfo.playerInfo = this;
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        if (PlayerPrefs.HasKey("LOCALCHARACTER"))
        {
            mySelectedCharacter = PlayerPrefs.GetInt("LOCALCHARACTER");
        }
        else
        {
            mySelectedCharacter = 0;
            PlayerPrefs.SetInt("LOCALCHARACTER", mySelectedCharacter);
        }
    }

    void Update()
    {
        switch (mySelectedCharacter)
        {
            case 0:
                avatarPrefabSelected = "Player Girl Photon 1";
                spawnPrefabNumber = 0;
                break;
            case 1:
                avatarPrefabSelected = "Player Girl Photon 2";
                spawnPrefabNumber = 1;
                break;
            case 2:
                avatarPrefabSelected = "Player Girl Photon 3";
                spawnPrefabNumber = 2;
                break;
            case 3:
                avatarPrefabSelected = "Player Girl Photon 4";
                spawnPrefabNumber = 3;
                break;
        }
    }
}
