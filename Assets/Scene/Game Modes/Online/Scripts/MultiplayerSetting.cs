﻿using Photon.Pun;
using UnityEngine;

public class MultiplayerSetting : MonoBehaviour
{
    public static MultiplayerSetting multiplayerSetting;

    public int maxPlayersInRoomAllowed;
    public int minPlayersInRoomToStartGame;
    public int menuScene;
    public int lobby;
    public int multiplayerWaitingRoomScene;
    public int multiplayerCharacterSelectScene;
    public int multiplayerGameScene;

    void Awake()
    {
        if (MultiplayerSetting.multiplayerSetting == null)
        {
            MultiplayerSetting.multiplayerSetting = this;
        }
        else
        {
            if (MultiplayerSetting.multiplayerSetting != this)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public void ChangeMultiplayerScene(int level) {
        multiplayerGameScene = 1 + level; //We add +1 because the value starts at 0 but it should starts at 1
    }
}
