﻿using Photon.Pun;
using System.IO;
using UnityEngine;

public class AvatarSetup : MonoBehaviour
{
    private PhotonView PV;
    public int characterValue;
    public GameObject mySelectedCharacter;
    public Animator animator;

    void Awake()
    {
        PV = GetComponent<PhotonView>();
    }

    private void OnEnable()
    {
        if (PV.IsMine)
        {
            PV.RPC("AddCharacter", RpcTarget.AllBuffered, PlayerInfo.playerInfo.mySelectedCharacter);
        }
    }

    [PunRPC]
    void AddCharacter(int characterIdentifier)
    {
        characterValue = characterIdentifier;
        mySelectedCharacter = Instantiate(PlayerInfo.playerInfo.allCharacters[characterIdentifier], transform.position, transform.rotation, transform);
    }
}
