﻿using ExitGames.Client.Photon;
using Photon.Chat;
using Photon.Pun;
using UnityEngine;
using UnityEngine.UI;

public class RiseChat : MonoBehaviour, IChatClientListener
{
    private string worldChat;
    private string playerName;

    public ChatClient chatClient;
    public Text connectionState;
    public Text msgInput;
    public Text msgArea;
    
    void Start()
    {
        Application.runInBackground = true;

        if (string.IsNullOrEmpty(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat))
        {
            Debug.Log("No chat ID provided");
            return;
        }

        connectionState.text = "Connecting to the server...";
        worldChat = "World";
        playerName = PlayerPrefs.GetString("vNick");

        GetConnected();
    }

    void Update()
    {
        chatClient.Service();
    }


    public void GetConnected()
    {
        Debug.Log("Chat App is trying to connect");
        chatClient = new ChatClient(this);
        chatClient.Connect(PhotonNetwork.PhotonServerSettings.AppSettings.AppIdChat, PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion, new AuthenticationValues(playerName));
        connectionState.text = "Connecting to Chat...";
    }

    public void SendMSG()
    {
        chatClient.PublishMessage(worldChat, msgInput.text);
    }

    public void DebugReturn(DebugLevel level, string message)
    {
    }

    public void OnDisconnected()
    {
        chatClient.Unsubscribe(new string[] { worldChat });
        chatClient.SetOnlineStatus(ChatUserStatus.Offline);
        connectionState.text = "Not connected";
    }

    public void OnConnected()
    {
        Debug.Log("********************************");
        chatClient.Subscribe(new string[] { worldChat });
        chatClient.SetOnlineStatus(ChatUserStatus.Online);
        connectionState.text = "You are connected";
    }

    public void OnChatStateChange(ChatState state)
    {
    }

    public void OnGetMessages(string channelName, string[] senders, object[] messages)
    {
        for (int i = 0; i < senders.Length; i++)
        {
            msgArea.text += senders[i] + " : " + messages[i] + "\n";
        }
    }

    public void OnPrivateMessage(string sender, object message, string channelName)
    {
    }

    public void OnSubscribed(string[] channels, bool[] results)
    {
        msgArea.text = playerName + " has joined the chat." + "\n";
    }

    public void OnUnsubscribed(string[] channels)
    {
    }

    public void OnStatusUpdate(string user, int status, bool gotMessage, object message)
    {
    }

    public void OnUserSubscribed(string channel, string user)
    {
    }

    public void OnUserUnsubscribed(string channel, string user)
    {
    }
}
