﻿using UnityEngine;

public class SpiroProximityController : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.name == "Player Girl Photon 2(Clone)")
        {
            SpiroControlledBlock.proximity = true;
        }
    }

    public void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.name == "Player Girl Photon 2(Clone)")
        {
            SpiroControlledBlock.proximity = true;
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.name == "Player Girl Photon 2(Clone)")
        {
            SpiroControlledBlock.proximity = false;
        }
    }
}
