﻿#if UNITY_STANDALONE_WIN
using System.IO.Ports;
#endif

using MoreMountains.CorgiEngine;
using UnityEngine;

public class SpiroControllerOffline : MonoBehaviour
{

#if UNITY_STANDALONE_WIN

    public static string arduinoValue;
    private float strongBreathValue;
    SerialPort stream = new SerialPort("com7", 9600); // 115200

    public float errorValue;

    private void Start()
    {
        errorValue = 0.4f;
    }

    /// <summary>
    /// Gets the strength of expiration. 0.0f means that the patient is not blowing.
    /// </summary>
    public float GetStrength()
    {
        return strongBreathValue;
    }

    /// <summary>
    /// Gets the BreathingState (expiration, inspiration, holding breath, ...).
    /// </summary>
    public BreathingState GetInputState()
    {
        if (this.strongBreathValue > errorValue)
        {
            return BreathingState.EXPIRATION;
        }
        else
        {
            return BreathingState.INSPIRATION; // HOLDING_BREATH
        }
    }

    void Update()
    {
        stream.Open();

        // timeout if no data received, in miliseconds
        stream.ReadTimeout = 100;
        if (stream.IsOpen)
        { // stream.available() > 0; ?
            try
            {
                // reads serial port
                arduinoValue = stream.ReadLine();

                this.strongBreathValue = float.Parse(arduinoValue);
                this.strongBreathValue = (this.strongBreathValue - 6);  // adapt the value received to match the breath
            }
            catch (System.Exception)
            {

            }
        }

        if (strongBreathValue >= errorValue)
        {
            SpiroControlledBlock.action = true;
            CharacterJetpack.action = true;
        }
        else
        {
            SpiroControlledBlock.action = false;
            CharacterJetpack.action = false;
        }

        stream.Close();
    }

#endif
}
