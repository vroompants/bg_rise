﻿#if UNITY_STANDALONE_WIN
using System.IO.Ports;
#endif

using UnityEngine;
using MoreMountains.CorgiEngine;
using Photon.Pun;

public class SpiroController : MonoBehaviourPunCallbacks
{
    private PhotonView PV;

#if UNITY_STANDALONE_WIN

    public static string arduinoValue;
    private float strongBreathValue;
    SerialPort stream = new SerialPort("com7", 9600); // 115200

    private float calibrationFactor = 1.0f;

    public float errorValue;

    void Start()
    {
        PV = GetComponent<PhotonView>();
        errorValue = 0.4f;
    }

    /// <summary>
    /// Gets the strength of expiration. 0.0f means that the patient is not blowing.
    /// </summary>
    public float GetStrength()
    {
        if (PV.IsMine)
        {
            return strongBreathValue;
        }
        else
        {
            return 0.0f;
        }
        
    }

    /// <summary>
    /// Gets the BreathingState (expiration, inspiration, holding breath, ...).
    /// </summary>
    public BreathingState GetInputState()
    {
        if (PV.IsMine && this.strongBreathValue > errorValue)
        {
            return BreathingState.EXPIRATION;
        }
        else
        {
            return BreathingState.INSPIRATION; // HOLDING_BREATH
        }


    }

    void Update()
    {
        if (PV.IsMine)
        {
            stream.Open();

            // timeout if no data received, in miliseconds
            stream.ReadTimeout = 100;
            if (stream.IsOpen)
            { // stream.available() > 0; ?
                try
                {
                    // reads serial port
                    arduinoValue = stream.ReadLine();

                    this.strongBreathValue = float.Parse(arduinoValue);
                    this.strongBreathValue = (this.strongBreathValue - 6);  // adapt the value received to match the breath
                }
                catch (System.Exception)
                {

                }
            }

            if (PV.IsMine)
            {
                if (strongBreathValue >= errorValue)
                {
                    Debug.Log("BOU");
                    CharacterJetpack.action = true;
                    CharacterHandleWeapon.action = true;
                    SpiroControlledBlock.action = true;
                }
                else
                {
                    Debug.Log("BOB");
                    CharacterJetpack.action = false;
                    CharacterHandleWeapon.action = false;
                    SpiroControlledBlock.action = false;
                }
            }

            //   Debug.Log ("ard " + arduinoValue);
            //	Debug.Log ("sbv " + this.strongBreathValue);
            stream.Close();
        }       
    }
#endif
}
