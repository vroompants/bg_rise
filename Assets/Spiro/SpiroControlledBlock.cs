﻿using UnityEngine;

public class SpiroControlledBlock : MonoBehaviour
{
    public static bool action = false;
    public static bool proximity = false;
    public float forceApplied = 8.0f;

    private Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // only need to adequately manipulate values of the rb2D from the inspector and set invisible collider only blocking the Spiro Cube
    void FixedUpdate()
    {
        if (action && proximity)
        {            
            rb.AddForce(transform.up * forceApplied);
            Debug.Log("Test");
        }
    }
}
