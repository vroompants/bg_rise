﻿using UnityEngine;
using Photon.Pun;

public class InputControllerScript : MonoBehaviourPunCallbacks
{
#if UNITY_STANDALONE_WIN

    private InputController_I inputcontroller;

    private PhotonView PV;

    // Use this for initialization
    void Start () {       
        PV = GetComponent<PhotonView>();

        if (PV.IsMine)
        {
            inputcontroller = new PepInputController_S(1.0f);
        }
    }
	
	void Update () {
        if (PV.IsMine)
        {
            inputcontroller.Update();
        }      
    }

    public InputController_I InputController
    {
        get { return inputcontroller; }
    }

#endif
}
